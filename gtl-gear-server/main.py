import wsgiref.handlers
import webapp2

from time import gmtime, strftime

from google.appengine.api import users
from google.appengine.ext import ndb

from django.template.loader import get_template
from google.appengine.ext.webapp import template

from google.appengine.ext.webapp.util import run_wsgi_app


class Item(ndb.Model):
	type_choices=list(["Android phone", "Android tablet", "Nokia phone", "iPhone", "iPad", "Robot", "Other"])
	higid = ndb.StringProperty()
	serial = ndb.StringProperty()
	imei = ndb.StringProperty()
	higtype = ndb.StringProperty(choices=type_choices)
	project = ndb.StringProperty() # Project for which the device is used
	borrower = ndb.StringProperty() # Borrower's name
	borrower_email = ndb.StringProperty() # Borrower's email
	google_account = ndb.StringProperty() # Google account configured on the phone
	name = ndb.StringProperty() # Device name
	remarks = ndb.TextProperty()

	def get_type_choices(self):
		return self.type_choices



class MainPage (webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			items = ndb.gql('SELECT * FROM Item ORDER BY higid').fetch()
			values = {
				'items': items,
				'type_choices': Item().get_type_choices(),
				'logout': users.create_logout_url("/"),
				'user': user.nickname(),
				'admin': users.is_current_user_admin()
			}
		else:
			values = {
				'url': users.create_login_url("/"),
			}
		self.response.out.write(unicode(template.render('templates/main.html', values)))


class AddItemPage (webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			values = {
				'type_choices': Item().get_type_choices(),
				'logout': users.create_logout_url("/"),
				'user': user.nickname(),
				'admin': users.is_current_user_admin()
			}
		else:
			values = {
				'url': users.create_login_url("/"),
			}
		self.response.out.write(unicode(template.render('templates/add_item.html',values)))
		
	def post(self):
		item = Item(
			higtype = self.request.get('type'),
			higid = self.request.get('id'),
			serial = self.request.get('serial'),
			imei = self.request.get('imei'),
			project = self.request.get('project'),
			google_account = self.request.get('google_account'),
			borrower = self.request.get('borrower'),
			borrower_email = self.request.get('borrower_email'),

			remarks = self.request.get('remarks'),
			name = self.request.get('name'))
		if item.higid != '': item.put()
		self.redirect('/')



class BorrowerPage(webapp2.RequestHandler):
	def unique_result(self,array):
		unique_results = []
		for obj in array:
			if obj.borrower not in unique_results:
				unique_results.append(obj.borrower)
		return unique_results

	def get(self):
		user = users.get_current_user()
		if user:
			items = ndb.gql('SELECT * FROM Item ORDER BY borrower').fetch()
			borrowers = self.unique_result(items)
			values ={
               	'items': items,
				'borrowers' : borrowers,
				'type_choices': Item().get_type_choices(),
				'logout': users.create_logout_url("/"),
				'user': user.nickname(),
				'admin': users.is_current_user_admin()
			}
		else:
			values = {
				'url': users.create_login_url("/"),
			}
		self.response.out.write(unicode(template.render('templates/borrowers.html',values)))



class TypePage(webapp2.RequestHandler):
	def unique_result(self,array):
		unique_results = []
		for obj in array:
			if obj.higtype not in unique_results:
				unique_results.append(obj.higtype)
		return unique_results

	def get(self):
		user = users.get_current_user()
		if user:
			items = ndb.gql('SELECT * FROM Item ORDER BY higtype').fetch()
			types = self.unique_result(items)
			values = {
					'items': items,
					'types': types,
                    'type_choices': Item().get_type_choices(),
					'logout': users.create_logout_url("/"),
					'user': user.nickname(),
					'admin': users.is_current_user_admin()
			}
		else:
			values = {
					'url': users.create_login_url("/")
			}
		self.response.out.write(unicode(template.render('templates/types.html', values)))



class ProjectPage(webapp2.RequestHandler):
	def unique_result(self,array):
		unique_results = []
		for obj in array:
			if obj.project not in unique_results:
				unique_results.append(obj.project)
		return unique_results

	def get(self):
		user = users.get_current_user()
		if user:
			items = ndb.gql('SELECT * FROM Item ORDER BY project').fetch()
			projects = self.unique_result(items)
			values = {
					'items': items,
                    'projects' : projects,
                    'type_choices': Item().get_type_choices(),
                    'logout': users.create_logout_url("/"),
                    'user': user.nickname(),
                    'admin': users.is_current_user_admin()
                    }
		else:
			values = {
            		'url': users.create_login_url("/"),
                    }
		self.response.out.write(unicode(template.render('templates/projects.html',values)))



class ModifyItemPage(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		id_value = self.request.get('id')
		if user:
			#items = ndb.gql('SELECT * FROM Item WHERE higid = :1', id_value).fetch()
			items = Item.query(Item.higid == id_value)
			items_iter = items.iter()
			if items_iter.has_next():
				values = { 
						'item': items_iter.next(),
						'logout': users.create_logout_url("/"),
						'user': user.nickname(),
						'admin': users.is_current_user_admin(),
						'type_choices': Item().get_type_choices() 
				}
			else:
				values = { 'error': 'could not find item' }
		else:
			values = {
            		'url': users.create_login_url("/"),
            }
		self.response.out.write(unicode(template.render('templates/modify_item.html', values)))

	def post(self):
		if not users.is_current_user_admin():
			self.redirect('/')

		user = users.get_current_user()
		higid = self.request.get('id')
		items = Item.query(Item.higid == higid)
		items_iter = items.iter()
		if items_iter.has_next():
			changes = "Changes on " + strftime("%Y-%m-%d %H:%M:%S", gmtime()) + "\n"
			item = items_iter.next()
			new_value = self.request.get('type')
			changes += check_modification ('higtype', item.higtype, new_value)
			item.higtype = self.request.get('type')

			new_value = self.request.get('serial')
			changes += check_modification ('serial', item.serial, new_value)
			item.serial = self.request.get('serial')

			new_value = self.request.get('imei')
			changes += check_modification ('imei', item.imei, new_value)
			item.imei = self.request.get('imei')

			new_value = self.request.get('name')
			changes += check_modification ('name', item.name, new_value)
			item.name = self.request.get('name')

			new_value = self.request.get('google_account')
			changes += check_modification ('google_account', item.google_account, new_value)
			item.google_account = self.request.get('google_account')

			item.project = self.request.get('project')

			new_value = self.request.get('borrower')
			changes += check_modification ('borrower', item.borrower, new_value)
			item.borrower = self.request.get('borrower')

			new_value = self.request.get('borrower_email')
			changes += check_modification ('borrower_email', item.borrower_email, new_value)
			item.borrower_email = self.request.get('borrower_email')
			
			item.remarks = self.request.get('remarks') + changes + "Done by " + user.nickname() + '\n'
			item.put()
		self.redirect('/')



def check_modification(category, old_value, new_value):
	if old_value == new_value:
		return ""
	else:
		return "" + category + " : " + str(old_value) + " --> " + str(new_value) + "\n"



app = webapp2.WSGIApplication(
	[
		(r'/projects/', ProjectPage), 
		(r'/types/', TypePage), 
		(r'/borrowers/', BorrowerPage),
		(r'/modify_item.html.*', ModifyItemPage),
		(r'/add_item.html.*', AddItemPage),
		(r'/', MainPage)
	], debug=True)

def main():
    run_wsgi_app(app)

if __name__ == "__main__":
    main()



